# Addison Global Backend Technical Assesement



## Implementation based on "Akka HTTP Quickstart for Scala" project se for more details https://developer.lightbend.com/guides/akka-http-quickstart-scala/index.html 
                           



## Introduction

* To run application - run AuthenticationServer
* To test application - run UserRoutesSpec 
                        or sbt test


## Glossary
* Credentials - A tuple of _username_ and _password_ that are used to authenticate a customer.
* User - Identifies a given customer within the system. For simplicity, it just contains _userId_ which will match the _username_ of the given customer.
* UserToken - Token granted to a user in order to perform further operations in the system. It is the concatenation of the _userId_ and the current time. For example: `user123_2017-01-01T10:00:00.000`

**Task** requirements / guidelines:

1. Implement an Actor/Service/Module that:
   * Validates the *Credentials* and return an instance of a *User*.
   * The *User* instance will always be returned with a random delay between 0 and 5000 milliseconds.
   * If the password matches the username in uppercase, the validation is a success, otherwise is a failure. Examples:
       * username: house , password: HOUSE => Valid credentials.
       * username: house , password: House => Invalid credentials.
   * The *userId* of the returned user will be the provided *username*.
   * This logic has to be encapsulated in a separate Actor/Service/Module.
    
2. Implement another Actor/Service/Module that:
   * Returns a *UserToken* for a given *User*.
   * The *UserToken* instance will always be returned with a random delay between 0 and 5000 milliseconds.
   * If the *userId* of the provided *User* starts with **A**, the call will fail.
   * The *token* attribute for the *User Token* will be the concatenation of the *userId* and the current date time in UTC: `yyyy-MM-dd'T'HH:mm:ssZ`.
        * Example: `username: house => house_2017-01-01T10:00:00Z`
   * This logic has to be encapsulated in a separate Actor/Service/Module.
   
3. Implement the *requestToken* function/method from the **SimpleAsyncTokenService** trait/interface in a way that:
   * Its logic is encapsulated in an Actor/Service/Module.
   * It makes use of the previously defined actors/services/modules for authenticating users and granting tokens:
        * It will first use the validation of the *Credentials* to obtain a *User*.
        * After that it will then use the actor/service/module to obtain a *UserToken*.
        * Finally, returns the *UserToken* to the original caller.
