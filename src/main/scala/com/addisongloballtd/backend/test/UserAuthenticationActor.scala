package com.addisongloballtd.backend.test

import java.text.SimpleDateFormat
import java.util.Calendar

import akka.actor.{ Actor, ActorLogging, Props }

final case class Credentials(username: String, password: String)
final case class User(userId: String)

object UserAuthenticationActor {
  case class AuthenticationResponse(userToken: Option[UserToken] = None, errorMessage: Option[ErrorMessage] = None)
  final case class ErrorMessage(description: String)
  final case class UserToken(token: String)

  def props: Props = Props[UserAuthenticationActor]
}

class UserAuthenticationActor extends Actor with ActorLogging {
  import UserAuthenticationActor._

  def receive: Receive = {
    case Credentials(username, password) => requestToken(Credentials(username, password))
  }

  private def requestToken(credentials: Credentials): Unit = {
    if (credentials.username.toUpperCase == credentials.password) {
      val user: User = authenticate(credentials)
      if (!user.userId.startsWith("A"))
        sender() ! AuthenticationResponse(userToken = Some(issueToken(user)))
      else
        sender() ! AuthenticationResponse(errorMessage = Some(ErrorMessage(s"ERROR: Cannot get the Token for UserID - ${user.userId}")))
    } else {
      sender() ! AuthenticationResponse(errorMessage = Some(ErrorMessage(s"Validation ERROR: for user - ${credentials.username}")))
    }
  }

  private def authenticate(credentials: Credentials): User = User(credentials.username)

  private def issueToken(user: User): UserToken = {
    def getTimeStamp: String = {
      val now = Calendar.getInstance().getTime
      val pattern = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
      pattern.format(now)
    }

    UserToken(s"${user.userId}_$getTimeStamp")
  }
}
