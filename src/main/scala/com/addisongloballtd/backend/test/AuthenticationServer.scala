package com.addisongloballtd.backend.test

import scala.concurrent.Await
import scala.concurrent.duration.Duration

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer


object AuthenticationServer extends App with UserRoutes {

  implicit val system: ActorSystem = ActorSystem("authenticationServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val userAuthenticationActor: ActorRef = system.actorOf(UserAuthenticationActor.props, "userAuthenticationActor")

  lazy val routes: Route = userRoutes

  Http().bindAndHandle(routes, "localhost", 8080)

  println(s"Server online at http://localhost:8080/")

  Await.result(system.whenTerminated, Duration.Inf)
}
