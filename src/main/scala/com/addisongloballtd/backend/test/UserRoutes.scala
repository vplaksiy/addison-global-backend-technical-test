package com.addisongloballtd.backend.test

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.pattern.ask
import akka.util.Timeout
import com.addisongloballtd.backend.test.UserAuthenticationActor._

import scala.concurrent.duration._

trait UserRoutes extends JsonSupport {

  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[UserRoutes])

  def userAuthenticationActor: ActorRef

  implicit lazy val timeout: Timeout = Timeout(5.seconds)

  lazy val userRoutes: Route =
    pathPrefix("requestToken") {
      pathEnd {
        post {
          entity(as[Credentials]) { credentials =>
            val resp =
              (userAuthenticationActor ? Credentials(credentials.username, credentials.password)).mapTo[AuthenticationResponse]

            onSuccess(resp) {
              case AuthenticationResponse(Some(token), _) =>
                log.info("Received Token for user [{}]", credentials.username)
                complete(StatusCodes.Created, token)
              case AuthenticationResponse(_, Some(e)) =>
                log.info("ERROR: Authentication failed for user [{}]", credentials.username)
                complete(StatusCodes.Unauthorized, e)
            }
          }
        }
      }
    }
}
