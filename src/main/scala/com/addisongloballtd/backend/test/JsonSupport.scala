package com.addisongloballtd.backend.test

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import UserAuthenticationActor.{ AuthenticationResponse, ErrorMessage, UserToken }
import spray.json.{ DefaultJsonProtocol, RootJsonFormat }


trait JsonSupport extends SprayJsonSupport {
  import DefaultJsonProtocol._

  implicit val credentialsFormat: RootJsonFormat[Credentials] = jsonFormat2(Credentials)
  implicit val userTokenFormat: RootJsonFormat[UserToken] = jsonFormat1(UserToken)

  implicit val actionPerformedJsonFormat: RootJsonFormat[ErrorMessage] = jsonFormat1(ErrorMessage)
  implicit val authenticationResponse: RootJsonFormat[AuthenticationResponse] = jsonFormat2(AuthenticationResponse)
}
