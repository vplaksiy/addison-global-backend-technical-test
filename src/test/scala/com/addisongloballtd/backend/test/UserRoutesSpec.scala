package com.addisongloballtd.backend.test

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, WordSpec }

class UserRoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest
    with UserRoutes {

  override val userAuthenticationActor: ActorRef =
    system.actorOf(UserAuthenticationActor.props, "userRegistry")

  lazy val routes: Route = userRoutes

  "UserRoutes" should {
    "return token if Credentials.password equals to Credentials.username in upper case (POST /requestToken)" in {
      val credentials = Credentials("username", "USERNAME")
      val credentialsEntity = Marshal(credentials).to[MessageEntity].futureValue

      val request = Post("/requestToken").withEntity(credentialsEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)

        contentType should ===(ContentTypes.`application/json`)

        entityAs[String] should startWith("{\"token\":\"username_")
      }
    }
  }

  "UserRoutes" should {
    "return Error Message if Credentials.password NOT equals to Credentials.username in upper case (POST /requestToken)" in {
      val credentials = Credentials("username", "UserName")
      val credentialsEntity = Marshal(credentials).to[MessageEntity].futureValue

      val request = Post("/requestToken").withEntity(credentialsEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Unauthorized)

        contentType should ===(ContentTypes.`application/json`)

        entityAs[String] should ===("{\"description\":\"Validation ERROR: for user - username\"}")
      }
    }
  }

  "UserRoutes" should {
    "return Error Message if Credentials.username start from character 'A' and have correct password (POST /requestToken)" in {
      val credentials = Credentials("Anna", "ANNA")
      val credentialsEntity = Marshal(credentials).to[MessageEntity].futureValue // futureValue is from ScalaFutures

      val request = Post("/requestToken").withEntity(credentialsEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Unauthorized)

        contentType should ===(ContentTypes.`application/json`)

        entityAs[String] should ===("{\"description\":\"ERROR: Cannot get the Token for UserID - Anna\"}")
      }
    }
  }
}
